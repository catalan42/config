#!/bin/tcsh
set confDir = $cwd
pushd ~
foreach ff ( .tmux.conf .cshrc .ctags .vimrc .gvimrc .bashrc .bash_profile )
  rm -f ${ff}
  ln -sv ${confDir}/${ff}
end
popd
