#!/bin/bash
echo ".bashrc - enter"

# set up vi command line editing mode
set -o vi  

# Get common bash/zsh aliases
source alias.bash

echo ".bashrc - exit"
