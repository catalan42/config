#!/bin/bash
echo ".zshrc - enter"

# set up vi command line editing mode
bindkey -v

# Get common bash/zsh aliases
source alias.bash

echo ".zshrc - exit"
